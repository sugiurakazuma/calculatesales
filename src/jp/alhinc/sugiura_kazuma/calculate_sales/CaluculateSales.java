package jp.alhinc.sugiura_kazuma.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;



public class CaluculateSales {
	public static void main(String[] args){
		if(args.length != 1){																		//コマンドライン引数が２つ以上、渡されていない場合
    		System.out.println("予期せぬエラーが発生しました");
    		return;
    	}
		HashMap<String,String> storeMap = new HashMap<String,String>();                                //マップstoreMapを作成
    	HashMap<String,Long> totalMap = new HashMap<String,Long>();
    	ArrayList<File> rcdList = new ArrayList<File>();
        if(!(putin(args[0], storeMap, totalMap))){
        	return;
        }
        if(!(aggres(args[0],totalMap,rcdList))){
        	return;
        }
		if(!(putout(args[0], storeMap,totalMap,rcdList))){
			return;
		}


	}
	private static Boolean putin(String path, HashMap<String,String> storeMap, HashMap<String,Long> totalMap ) {										//入力メソッド
		BufferedReader br = null;
    	
    	try{
    	    File file = new File(path, "branch.lst");
    	    if (!(file.exists())) {
    	        System.out.println("支店定義ファイルが存在しません");													//Mapに支店コードと支店名を入れるメソッド
    	        return false;
    	    }
    	    FileReader fr = new FileReader(file);
    	    br = new BufferedReader(fr);
    	    String line;
    	    while((line = br.readLine()) != null){
    	        String[] code = line.split(",");                                                    //支店コードと支店名を区切る
    	        storeMap.put(code[0],code[1]);                                                        //code[0]にkey code[1]に値を入れる
    	        totalMap.put(code[0],0L);                                                            //totalmapに支店コードを入れる
    	        if(!(code[0].matches("[0-9]{3}"))){
    	        	System.out.println("支店定義ファイルのフォーマットが不正です");
    	        	return false;
    	        }
    	        if(code.length != 2){												//支店定義ファイルの要素数チェック
    	        	System.out.println("支店定義ファイルのフォーマットが不正です");					//支店定義ファイルの要素数が１，３行の場合
    	        	return false;
    	        }
    	    }
    	}catch(IOException e){
    	    System.out.println("エラー");
    	    return false;
    	}finally{
    	    if(br != null){
    	        try{
    	            br.close();
    	        }catch(IOException e){
    	            System.out.println("closeできませんでした");
    	            return false;
    	        }
    	    }
    	}
    	return true;
	}
	private static Boolean aggres(String path,HashMap<String,Long> totalMap,ArrayList<File> rcd) {										//集計メソッド
		String dir = path;
        File file = new File(dir);
        File[] filename = file.listFiles();
        for (int i = 0;i<filename.length;i++){
        	if(filename[i].getName().matches("^[0-9]{8}.rcd$")){
        		rcd.add(filename[i]);
        		if(!(filename[i].isFile())){
        			System.out.println("売上ファイル名が連番になっていません");
        		}
        	
        	}
        }
        BufferedReader br2 = null;
        for(int i=0;i<(rcd.size());i++){
        	try{
	    		ArrayList<String> salesList = new ArrayList<String>();                            //ArrayList salesListの作成
	    		FileReader fr2 = new FileReader(rcd.get(i));
	    		br2 = new BufferedReader(fr2);
	            String line2;
	            while((line2 = br2.readLine()) != null){
	            	salesList.add(line2);
	                if(!(line2.matches("[0-9]{0,9}"))){                                            //売上金額が数字以外
						System.out.println("指定定義ファイルのフォーマットが不正です。");
						return false;
					}
	            }
	            if(salesList.size()!= 2){
	        		System.out.println(rcd.get(i).getName() + "のフォーマットが不正です");
	        		return false;
	        	}
	            if(totalMap.get(salesList.get(0))!=null) {
	                System.out.print("");
	            }else {
	                System.out.print(rcd.get(i).getName()+"の支店コードが不正です");
	                return false;
	            }
	            long sum = 0L;
	            sum = totalMap.get(salesList.get(0)) +Long.parseLong(salesList.get(1));
	            totalMap.put(salesList.get(0),sum);
	            int keta = 0;
	            while(sum>0) {
	                sum = sum/10;
	                keta++;
	            }
	            if(!(keta < 10)) {
	                System.out.print("合計金額が10桁を超えました");
	                return false;
	            }
        	}
        	catch(IOException e){
        		System.out.println("");
        		return false;
	        }finally{
        		if(br2 != null){
				    try{
				        br2.close();
				    }catch(IOException e){
				    	System.out.println("closeできませんでした");
				    	return false;
				    }
        		}
	        }
        }
        return true;
    }

	private static Boolean putout(String path,HashMap<String,String> storeMap, HashMap<String,Long> totalMap,ArrayList<File> rcd) {									//出力メソッド
		try{
	    	File newFile = new File(path,"branch.out");
	    	newFile.createNewFile();
	    	FileWriter fw = new FileWriter(newFile);
	    	BufferedWriter bw = new BufferedWriter(fw);
	    	for(java.util.Map.Entry<String, String> map : storeMap.entrySet()) {
	    		bw.write(map.getKey() + "," + map.getValue());
	    		bw.write(","+totalMap.get(map.getKey()));
	    		bw.write("\r\n");
	    	}
	    	bw.close();
	   	}catch(IOException e){
	    	System.out.println(e);
	    	return false;
	   	}
	    for(int a = 0; a<(rcd.size())-1;a++){
	    	String filename1 = rcd.get(a).getName();
	    	String filename2 = rcd.get(a+1).getName();
	    	String code = filename1.substring(0,8);
	    	String code2 = filename2.substring(0,8);
	    	int rcdNumber1 = Integer.parseInt(code);
	    	int rcdNumber2 = Integer.parseInt(code2);
	    	if(rcdNumber2-rcdNumber1!=1 || rcdNumber1 > rcd.size() ){
	    		System.out.println("売上ファイル名が連番になっていません");
	    		return false;
	    	}
		}
	    	return true;
	}
	
}

