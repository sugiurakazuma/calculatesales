package jp.alhinc.sugiura_kazuma.calculate_sales;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;


public class Syuukei {
	public static void main(String[] args){
		System.out.println("ここにあるファイルを開きます => " + args[0]);
		BufferedReader br = null;
		try{
			File file = new File("C:\\Users\\sugiura.kazuma\\Desktop\\売り上げ集計課題", "00000001.rcd");
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			while((line = br.readLine()) != null){
				System.out.println(line);
			}
		}catch(IOException e){
			System.out.println("エラー");

		}finally{
				if(br != null){
					try{
						br.close();
					}catch(IOException e){
						System.out.println("closeできませんでした。");
	
					}
				}
				
		}
	}
}
